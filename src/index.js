import { ShadowComponent } from '@aegis-framework/pandora';
import { pwnedPassword } from 'hibp';
import zxcvbn from 'zxcvbn';

/**
 * @class PasswordInput - A simple password input component that enforces the
 * recommended guidelines from OWASP for password strength and validates it
 * against the Pwned passwords available on Have I been pwned.
 *
 * OWASP Resources: https://github.com/OWASP/ASVS/blob/master/4.0/en/0x11-V2-Authentication.md#v21-password-security-requirements
 */
class PasswordInput extends ShadowComponent {

    constructor (...args) {
        super (...args);

        this.props = {
			// Input Attributes
            'required': false,
			'autocomplete': 'new-password',
			'name': 'password',
			'disabled': false,
			'placeholder': 'Password',


			// Passwords shorter than 12 characters are considered weak
			'min-length': 12,

			// It is important to set a maximum password length to prevent long password Denial of Service attacks.
			'max-length': 128,

			'min-score': 75,

			'scores': {
				0: 'very-weak',
				25: 'very-weak',
				50: 'weak',
				75: 'reasonable',
				100: 'strong'
			},

			// Whether to show the password strength meter or not
			'strength-meter': false,
			'hibp-validation': false
        };

        this.state = {
			// Whether the password is currently visible or not
			visible: false,

			// The score in a 0 to 100 scale of the password's strength
			score: 0
        };

        this.setStyle ({
            ':host': {
				'border': '1px solid rgba(0, 0, 0, 0.2)',
				'display': 'inline-block'
            },
            'div': {
                'display': 'flex',
				'flex-direction': 'row',
				'justify-content': 'flex-start'
			},
			'.wrapper': {
				'flex-direction': 'column',
				'overflow': 'hidden'
			},
            'button': {
                'border': 'none',
                'background': 'inherit',
                'cursor': 'pointer',
                'outline': 'none',
				'padding': 0,
				'padding-right': '1em',
				'color': '#666'
			},
			'button[disabled]': {
				'background': '#eee'
			},
            'button:focus': {
                'outline': 'none'
            },
            'button:active': {
                'outline': 'none'
            },
            'button::-moz-focus-inner': {
                'border': 0
            },
            'input': {
                'border': 'none',
				'padding': '0.5em 1em',
				'width': '100%',
				'outline': 'none'
			},
			'input[disabled]': {
				'background': '#eee'
			},
            'span:not(:empty)': {
                'padding': '1rem'
			},
			'progress': {
				'height': '0.15rem',
				'-webkit-appearance': 'none',
				'-moz-appearance': 'none',
				'appearance': 'none',
				'box-shadow': 'none',
				'border': 'none',
				'width': '100%'
			},
			'progress[data-strength="very-weak"]::-webkit-progress-value': {
				'background': '#b71c1c'
			},
			'progress[data-strength="weak"]::-webkit-progress-value': {
				'background': '#ffc107'
			},
			'progress[data-strength="reasonable"]::-webkit-progress-value': {
				'background': '#8bc34a'
			},
			'progress[data-strength="strong"]::-webkit-progress-value': {
				'background': '#4caf50'
			},
			'progress[data-strength="very-weak"]::-moz-progress-bar': {
				'background': '#b71c1c'
			},
			'progress[data-strength="weak"]::-moz-progress-bar': {
				'background': '#ffc107'
			},
			'progress[data-strength="reasonable"]::-moz-progress-bar': {
				'background': '#8bc34a'
			},
			'progress[data-strength="strong"]::-moz-progress-bar': {
				'background': '#4caf50'
			}
        });
    }

    didMount () {
		// Every time the button is clicked, we'll toggle the visibility state
		// of the password input
        this.dom.querySelector ('button').addEventListener ('click', (event) => {
			event.preventDefault ();
			event.stopPropagation ();

            this.setState ({
                visible: !this.state.visible
			});

			this.focus ();
		});

		if (this.props["strength-meter"] === true) {
			this.dom.querySelector ('input').addEventListener ('input', (event) => {
				const value = event.target.value;
				const progressBar = this.dom.querySelector ('progress');

				const score = (zxcvbn (value).score / 4) * 100;

				if (value.length > 0) {
					progressBar.value = score > 0 ? score : 25;
					progressBar.dataset.strength = this.props.scores[score];
				} else {
					progressBar.value = 0;
					delete progressBar.dataset.strength;
				}
			});
		}

		this.dom.querySelector ('input').addEventListener ('blur', (event) => {
			if (event.relatedTarget) {
				if (event.relatedTarget.dataset.action !== 'toggle-visibility') {
					this.setState ({
						visible: false
					});
				}
			}
		});

        return Promise.resolve ();
	}


    onStateUpdate () {
		// Set the right input type depending on the visibility state. If the
		// password is visible, the `text` type will be used. If it is not
		// visible, the default `password` type will be used.
		this.dom.querySelector ('input').type = this.state.visible ? 'text' : 'password';

		// Update the text on the button
        this.dom.querySelector ('button').innerText = this.state.visible ? 'Hide' : 'Show';

		return Promise.resolve ();
	}

	onPropsUpdate () {
		this.forceRender ();
		return this.didMount ();
	}

    get value () {
        return this.dom.querySelector ('input').value;
    }

    set value (val) {
        this.dom.querySelector ('input').value = val;
	}

	get name () {
		return this.props.name;
	}

	set name (value) {
		this.setProps ({
			name: value
		});
	}

    reset () {
        this.dom.querySelector ('input').reset ();
	}

	focus () {
		this.dom.querySelector ('input').focus ();
	}

	blur () {
		this.dom.querySelector ('input').blur ();
	}

	click () {
		this.dom.querySelector ('input').click ();
	}

	select () {
		this.dom.querySelector ('input').select ();
	}


    validate () {
        return new Promise ((resolve, reject) => {
            const value = this.value;
            const errors = [];

			// Validate password lengths
            if (value.length < this.props['min-length']) {
                errors.push (`The password is too short, it should be at least ${this.props['min-length']} characters long.`);
            } else if (value.length > this.props['max-length']) {
                errors.push (`The password is too long, it should be at most ${this.props['max-length']} characters long.`);
			}

			const scoreWarning = zxcvbn(value).feedback.warning;

			if (scoreWarning) {
				errors.push (scoreWarning);
			}

            if (value.length > 0) {
                pwnedPassword (value).then (numPwns => {
                    // Check if the password has been pwned before
                    if (numPwns) {
                        errors.push (`The password has been pwned ${numPwns} times, using it is not safe.`);
                    }

                    if (errors.length > 0) {
                        reject (errors);
                    } else {
                        resolve ();
                    }
                }).catch (err => {
                    console.error (err);

                    //reject (err);
                });
            } else {
                if (errors.length > 0) {
                    reject (errors);
                } else {
                    resolve ();
                }
            }
        });
    }

    render () {
		return `
			<div class="wrapper">
				<div>
					<input
						type="${ this.state.visible ? 'text' : 'password' }"
						name="${ this.props.name }"

						autocomplete="${ this.props.autocomplete }"

						placeholder="${this.props.placeholder}"

						min="${this.props['min-length']}"
						max="${this.props['max-length']}"

						${ this.props.required ? 'required' : '' }
						${ this.props.disabled ? 'disabled' : '' }
					>
					<button type="button" data-action="toggle-visibility" ${ this.props.disabled ? 'disabled' : '' }>${ this.state.visible ? 'Hide' : 'Show' }</button>
				</div>
				${ this.props["strength-meter"] === true ? '<progress min="0" max="100" value="0"></progress>' : '' }
			</div>
        `;
    }
}

PasswordInput.tag = 'password-input';

export default PasswordInput;