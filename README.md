# Password Input Component

A simple password input with Hide/Show functionality built in and optional password strength indicator and Have I been pwned checks

## Props

| Name | Type | Description |
| ---- | ---- | ----------- |
| `required` | `boolean` | |
| `autocomplete` | `boolean` | |
| `min-length` | `boolean` | |
| `max-length` | `boolean` | |
